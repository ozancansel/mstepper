#include <Arduino.h>
#include <stepper.h>
#include <stepperdriver.h>
#include <shell.h>
#include <string.h>
#include <stdlib.h>
#include <parser.h>
#include <settings.h>

#define HOST_SERIAL Serial1
#define ERR_POS_NOT_SPECIFIED 1
#define ERR_SPEED_NOT_SPECIFIED 2
#define ERR_VALUE_SHOULD_BE_POSITIVE 3
#define ERR_ACCEL_NOT_SPECIFIED 4

#define OK_MESSAGE ';'

class UsbStream : public IStream
{
  public:
    UsbStream(USBSerial *serial) : m_serial(serial) {}
    int available() { return m_serial->available(); }
    int read() { return m_serial->read(); }

  private:
    USBSerial *m_serial;
};

class UartStream : public IStream
{
  public:
    UartStream(HardwareSerial *serial) : m_serial(serial) {}
    int available() { return m_serial->available(); }
    int read() { return m_serial->read(); }

  private:
    HardwareSerial *m_serial;
};

void reset();
void stop(bool soft);

Stepper st;

extern plan_t st_plan;
extern int State;
extern Settings settings;
long calculated = 0;

void sendOkMessage();
void sendErrMessage(int code);
void onCommandReceived(char **cmd, int length);
void onI2cCommandReceived(char **cmd, int length);

Shell sh(new UartStream(&Serial1), onCommandReceived);
Shell sh2(new UsbStream(&Serial), onCommandReceived);

void setup()
{
    Serial1.begin(9600);

    loadSettings();

    //Reset
    reset();

    st.setAccel(settings.acceleration);
    st.posInSteps = 0;

    st_init();
    st_set_stepper(&st);
}

void loop()
{
    sh.listen();
}

void onCommandReceived(char **cmd, int length)
{

    Parser p(cmd, length);

    if (p.equal("move"))
    {

        bool sync = false;

        if (!p.hasNext())
        {
            sendErrMessage(ERR_POS_NOT_SPECIFIED);
            return;
        }

        //Retrieve next word
        p.getNext();

        //If any option is specified
        if (p.isOption())
        {
            //If --sync option is specified
            if (p.equal("--sync"))
                sync = true;

            //Retrieve next word
            p.getNext();
        }

        long pos = atol(p.current());

        if (!p.hasNext())
        {
            sendErrMessage(ERR_SPEED_NOT_SPECIFIED);
            return;
        }

        long speed = atol(p.getNext());

        if (speed <= 0)
        {
            sendErrMessage(ERR_VALUE_SHOULD_BE_POSITIVE);
            return;
        }

        st_move(pos, speed);

        //If sync option is specified
        if (sync)
        {
            while (State & MOVING_BIT)
            {
                delay(1);
            }
        }
        sendOkMessage();
    }
    else if (p.equal("jog"))
    {

        bool sync = false;

        p.getNext();

        //If any option is specified
        if (p.isOption())
        {
            //If --sync option is specified
            if (p.equal("--sync"))
                sync = true;

            //Retrieve next word
            p.getNext();
        }

        //If position not specified
        if (!p.hasNext())
        {
            sendErrMessage(ERR_POS_NOT_SPECIFIED);
            return;
        }

        long pos = atol(p.current());

        if (!p.hasNext())
        {
            sendErrMessage(ERR_SPEED_NOT_SPECIFIED);
            return;
        }

        long speed = atol(p.getNext());

        if (speed <= 0)
        {
            sendErrMessage(ERR_VALUE_SHOULD_BE_POSITIVE);
            return;
        }

        st_move(st.posInSteps + pos, speed);
        sendOkMessage();
    }
    else if (p.equal("pos"))
    {
        HOST_SERIAL.print("pos ");
        HOST_SERIAL.println(st.posInSteps);
    }
    else if (p.equal("set"))
    {

        //Iterate over commands
        while (p.hasNext())
        {
            //Iterate
            p.getNext();
            if (!p.isOption())
                continue;

            //If option is --accel
            if (p.equal("--accel"))
            {
                //If accel is not specified
                if (!p.hasNext())
                {
                    sendErrMessage(ERR_ACCEL_NOT_SPECIFIED);
                    continue;
                }

                long accel = atol(p.getNext());

                if (accel <= 0)
                {
                    sendErrMessage(ERR_VALUE_SHOULD_BE_POSITIVE);
                    continue;
                }

                //Set acceleration
                st.setAccel(accel);
                st_set_stepper(&st);
                settings.acceleration = accel;
                saveSettings();

                sendOkMessage();
            }
            else if (p.equal("--pos"))
            {
                if (!p.hasNext())
                {
                    sendErrMessage(ERR_POS_NOT_SPECIFIED);
                    continue;
                }

                //Set position
                st.posInSteps = atol(p.getNext());

                sendOkMessage();
            }
        }
    }
    else if (p.equal("running"))
    {
        if (State & MOVING_BIT)
            HOST_SERIAL.println(F("yes"));
        else
            HOST_SERIAL.println(F("no"));
    }
    else if (p.equal("stop"))
    {

        if (p.hasNext())
        {
            p.getNext();
            bool soft = false;
            bool sync = false;

            //If motor is moving and soft stop is specified
            if (p.exist("--soft"))
                soft = true;
            if (p.exist("--sync"))
                sync = true;

            //If soft option is not specified
            if (!soft)
                stop(false);
            else
            {
                stop(true);

                if (sync)
                {
                    while (State & MOVING_BIT)
                    {
                        delay(1);
                    }
                }
            }

            //Send ok message
            sendOkMessage();
        }
        else
        {
            stop(false);
            sendOkMessage();
        }
    }
    else if (p.equal("settings"))
    {
        //If has no option, return
        if (!p.hasNext())
            return;

        while (p.hasNext())
        {
            p.getNext();

            if (p.equal("accel") || p.equal("--all"))
            {
                HOST_SERIAL.print("accel=");
                HOST_SERIAL.println(settings.acceleration);
            }
            if (p.equal("i2cAddr") || p.equal("--all"))
            {
                HOST_SERIAL.print("i2cAddr=");
                HOST_SERIAL.println(settings.i2cAdress);
            }
            if (p.equal("mode") || p.equal("--all"))
            {
                HOST_SERIAL.print("mode=");
                HOST_SERIAL.println(settings.mode);
            }
            if (p.equal("algorithm") || p.equal("--all"))
            {
                HOST_SERIAL.print("algorithm=");
                HOST_SERIAL.println(settings.algorithm);
            }
        }
    }
    else if (p.equal("reset"))
    {
        reset();
    }
}

void sendOkMessage()
{
    HOST_SERIAL.println(OK_MESSAGE);
}

void sendErrMessage(int errCode)
{
    HOST_SERIAL.print("ERR:");
    HOST_SERIAL.println(errCode);
}

void stop(bool soft)
{
    //If motor is not moving
    if (!(State & MOVING_BIT))
    {
        return;
    }

    //If option is enabled. then stopping with decelerate
    if (soft)
    {
        //If motor is on cruise
        if (st_plan.steps_remaining >= st_plan.accel_point)
        //If motor is accelerating
        {
            st_plan.decel_point = st_plan.steps_total - st_plan.steps_remaining;
            st_plan.steps_remaining = st_plan.decel_point;
        }
        else if (st_plan.steps_remaining > st_plan.decel_point)
        {
            st_plan.steps_remaining = st_plan.decel_point;
        }
        else
        {
            st_plan.steps_remaining = 0;
        }
    }
    else
    {
        st_plan.steps_remaining = 0;
    }
}

void reset()
{
    //Reset position
    stop(false);
    st.posInSteps = 0;
    HOST_SERIAL.println(F("[MStepper Driver v1.0]"));
}