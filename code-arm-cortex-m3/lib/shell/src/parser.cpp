#include "parser.h"
#include <string.h>
#include <Arduino.h>

Parser::Parser(char** cmd , int length) 
:
    m_cmd(cmd) ,
    m_idx(0) ,
    m_length(length)
{

}

char* Parser::getNext(){
    if(!hasNext())
        return NULL;
    m_idx++;
    return m_cmd[m_idx];
}

bool Parser::hasNext(){
    return m_idx < (m_length - 1);
}

char* Parser::cmd(){
    return m_cmd[0];
}

bool Parser::isOption(){

    if(strlen(current()) < 2)
        return false;

    return current()[0] == '-' && current()[1] == '-';
}

bool Parser::exist(char* option){
    for(int i = 0; i < m_length; i++){
        if(strcmp(m_cmd[i] , option) == 0)
            return true;
    }

    return false;
}

bool Parser::equal(const char* str){
    return strcmp(m_cmd[m_idx] , str) == 0;
}

char* Parser::current(){
    return m_cmd[m_idx];
}

void Parser::reset(){
    m_idx = 0;
}