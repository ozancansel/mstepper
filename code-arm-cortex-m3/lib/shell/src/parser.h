#ifndef PARSER_H
#define PARSER_H

class Parser    {

    public:
        Parser(char** cmd , int length = -1);
        char*   getNext();
        bool    hasNext();
        char*   cmd();
        bool    isOption();
        bool    equal(const char* str);
        char*   getValue();
        void    reset();
        char*   current();
        bool    exist(char* option);

    private:

        char**  m_cmd;
        int     m_idx;
        int     m_length;

};

#endif