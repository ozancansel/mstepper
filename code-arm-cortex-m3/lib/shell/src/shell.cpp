#include "shell.h"
#include <string.h>
#include <stdlib.h>
#include <Arduino.h>

Shell::Shell(IStream *stream, void (*callback)(char **, int))
    : m_stream(stream),
      m_cb(callback)
{
    reset();
}

void Shell::listen()
{
    //If there is any available bytes
    while (m_stream->available())
    {
        int byte = m_stream->read();

        feed(byte);
    }
}

void Shell::feed(int byte)
{
    //If command is ended
    if (byte == '\n' || byte == '\r')
    {

        if (m_idx == 0)
            return;

        //Terminate buffer
        m_buffer[m_idx] = '\0';

        char *dup;
        char *token = (char *)"f";
        char **args;
        int length = 0;

        args = (char **)malloc(sizeof(char **) * 12);

        dup = strdup(m_buffer);
        const char delimiters[] = " ";

        while (1)
        {
            // args = (char**)realloc(args , sizeof(char**) * length + 1);
            token = strsep(&dup, delimiters);
            if (token == NULL)
                break;

            args[length] = token;
            length++;
            if (length >= 12)
                break;
        }

        //Trigger callback function
        m_cb(args, length);
        // for(int i = 0; i < length; i++){
        //     free(args[i]);
        // }
        free(args);

        //Reset buffer
        reset();
    }
    else
    {
        //Command is receiving
        m_buffer[m_idx] = byte;
        m_idx++;
        Serial.println("new");
    }
}

void Shell::reset()
{
    m_idx = 0;
}