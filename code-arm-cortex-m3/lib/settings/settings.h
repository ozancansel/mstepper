#ifndef SETTINGS_H
#define SETTINGS_H

#include <Arduino.h>
#define ALGORITHM_1     1
#define MODE_POSITION   1

struct Settings{
    int     acceleration;
    int     mode;
    int     algorithm;
    int     i2cAdress;
};

void    loadSettings();
void    saveSettings();
int     readInt(int addr);
void    writeInt(int addr, int num);

extern Settings settings;

#endif