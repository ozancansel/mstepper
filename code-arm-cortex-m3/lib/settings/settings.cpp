#include "settings.h"
#include <EEPROM.h>

Settings settings;

void loadSettings()
{
    settings.acceleration = readInt(0);
    settings.algorithm = readInt(4);
    settings.i2cAdress = readInt(8);
    settings.mode = readInt(12);
}

void saveSettings()
{
    writeInt(0 , settings.acceleration);
    writeInt(4 , settings.algorithm);
    writeInt(8, settings.i2cAdress);
    writeInt(12 , settings.mode);
}

int readInt(int addr)
{
    union {
        byte b[4];
        int num;
    } data;

    for (int i = 0; i < 4; i++)
    {
        data.b[i] = EEPROM.read(addr + i);
    }

    return data.num;
}

void writeInt(int addr, int num)
{
    union {
        byte b[4];
        int num;
    } data;

    data.num = num;

    for (int i = 0; i < 4; i++)
    {
        EEPROM.write(addr + i , data.b[i]);
    }
}