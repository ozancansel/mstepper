#include "stepperdriver.h"
#include "stepper.h"
#include <Arduino.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

volatile plan_t st_plan;
Stepper        *stepper;
int            State;
volatile float q;
volatile float m_period;
volatile float m_accel_factor;

HardwareTimer t1(1);
HardwareTimer t2(2);

void st_init()
{
  m_period = 0;
  pinMode(PB9 , OUTPUT);
  pinMode(PB8 , OUTPUT);

    t1.setChannel1Mode(TIMER_OUTPUT_COMPARE);
    t1.setCompare(TIMER_CH1 , 1);
    t1.attachCompare1Interrupt(st_rising_handler);

    t2.setChannel1Mode(TIMER_OUTPUT_COMPARE);
    t2.setCompare(TIMER_CH1 , 1);
    t2.attachCompare1Interrupt(st_falling_handler);

    t2.setPeriod(8);
}

void st_set_stepper(Stepper *st)
{
  stepper = st;
  m_accel_factor = stepper->accel / ((long double)TIMER_FREQUENCY * TIMER_FREQUENCY);
}

void st_calculate()
{
  if (st_plan.steps_remaining > st_plan.accel_point)
  {
    // q = m_accel_factor * m_period * m_period;
    // m_period = m_period * (1 - (q + q * q));
    m_period = m_period * (1 - m_accel_factor * m_period * m_period);
  }
  else if (st_plan.steps_remaining < st_plan.decel_point)
  {
    // q = m_accel_factor * m_period * m_period;
    // m_period = m_period * (1 + (q + q * q));
    m_period = m_period * (1 + m_accel_factor * m_period * m_period);
  }
}

void st_decelerate(){
  //Is moving
  //And if accelerating or cruise bit
  if((State & MOVING_BIT) && (State & (ACCELERATING_BIT | CRUISE_BIT))){
    st_plan.steps_remaining -= st_plan.steps_remaining - st_plan.decel_point;
    st_plan.decel_point = st_plan.steps_remaining;
  }
}

void st_hard_stop(){
  st_plan.steps_remaining = 1;
}

void st_move(int32_t pos, int32_t speed)
{
  //Calculate how many steps are required to move to the pos
  st_plan.steps_remaining = abs(pos - (stepper->posInSteps));
  st_plan.steps_total = st_plan.steps_remaining;
  //Calculate initial period
  m_period = TIMER_FREQUENCY / sqrtf(2 * stepper->accel);
  //Calculate deceleration
  st_plan.decel_point = speed * speed / (2.0 * stepper->accel);

  uint32_t half = st_plan.steps_remaining * 0.5;

  if (st_plan.decel_point > half)
  {
    st_plan.decel_point = half;
  }

  st_plan.accel_point = st_plan.steps_remaining - st_plan.decel_point;

  if (pos - stepper->posInSteps > 0)
  {
    // st_plan.direction = 1;
    State |= DIRECTION_BIT;
    // PORTD |= stepper->dirMask;
    digitalWrite(PB9 , HIGH);
  }
  else
  {
    // st_plan.direction = 0;
    State &= ~DIRECTION_BIT;
    // PORTD &= ~(stepper->dirMask);
    digitalWrite(PB9 , LOW);
  }

  State |= MOVING_BIT | ACCELERATING_BIT;

  t1.pause();
  t1.setPeriod(m_period);
  t1.refresh();
  t1.resume();
}

void     st_rising_handler(){
  if (st_plan.steps_remaining == 0)
  {
    // TIMSK1 &= ~(1 << OCIE1A);
    t1.pause();
    State &= ~MOVING_BIT;
    return;
  }
  else
  {
    // OCR1A = m_period;
    // t1.pause();
    t1.setPeriod(m_period);
    // t1.refresh();
    // t1.resume();
  }

    //If rotating cw direction, then increment steps position
  if(State & DIRECTION_BIT)
  {
    stepper->posInSteps++;
  }
  else
  { //If rotating ccw direction, then decrement steps position
    stepper->posInSteps--;
  }

  // PORTD |= stepper->pulseMask;
  digitalWrite(PB8 , HIGH);
  // TIMSK2 |= (1 << OCIE2A);
  t2.resume();
  st_plan.steps_remaining--;
}

void     st_falling_handler(){
  //Disable low edge timer
  // TIMSK2 &= ~(1 << OCIE2A);
  t2.pause();
  //Apply opposite of pulsemask
  // PORTD &= ~(stepper->pulseMask);
  digitalWrite(PB8 , LOW);
  //Calculate next pulse period
  st_calculate();
}
