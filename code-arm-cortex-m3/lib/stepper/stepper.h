#ifndef STEPPER_H
#define STEPPER_H

#include <stdint.h>

class Stepper   {

    public:


        Stepper();
        void    setAccel(double val);

        int     running;
        int     pulseMask;
        int     dirMask;
        unsigned int    accel;
        long        posInSteps;

};

#endif