#ifndef STEPPER_DRIVER_H
#define STEPPER_DRIVER_H

#include <stdint.h>

#define TIMER_FREQUENCY 1000000UL

#define MOVING_BIT          (1 << 0)
#define ACCELERATING_BIT    (1 << 1)
#define DECELERATING_BIT    (1 << 2)
#define CRUISE_BIT          (1 << 3)
#define DIRECTION_BIT       (1 << 4)

class Stepper;

typedef struct {
    unsigned long steps_total;
    unsigned long steps_remaining;
    unsigned long accel_point;
    unsigned long decel_point;
} plan_t;

void     st_init();
void     st_move(int32_t pos , int32_t speed);
void     st_decelerate();
void     st_hard_stop();
void     st_calculate();
void     st_set_stepper(Stepper* stepper);
void     st_rising_handler();
void     st_falling_handler();

#endif