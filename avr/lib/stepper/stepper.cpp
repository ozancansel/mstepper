#include "stepper.h"
#include "stepperdriver.h"
#include <Arduino.h>

Stepper::Stepper()
:
    running(0) ,
    pulseMask(0) ,
    dirMask(0) ,
    enableMask(0) ,
    accel(0) ,
    posInSteps(0)
{
    
}

void Stepper::setAccel(long val){
    accel = val;
}