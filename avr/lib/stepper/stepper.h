#ifndef STEPPER_H
#define STEPPER_H

#include <stdint.h>

class Stepper   {

    public:


        Stepper();
        void    setAccel(long val);

        int     running;
        int     pulseMask;
        int     dirMask;
        int     enableMask;
        long    accel;
        long    posInSteps;

};

#endif