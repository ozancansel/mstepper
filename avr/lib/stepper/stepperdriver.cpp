#include "stepperdriver.h"
#include "stepper.h"
#include <Arduino.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <limit.h>
#include <settings.h>
#include <helper.h>

volatile plan_t st_plan;
Stepper *stepper;
volatile int State;
volatile float q;
volatile float m_period;
volatile float m_accel_factor;
volatile bool calc_req;
volatile long speed;
extern Settings currentSettings;

void st_init()
{
  m_period = 0;
  State = 0;
  calc_req = false;

  DDRD = (1 << PD2) | (1 << PD3) | (1 << PD7);
  PORTD = (1 << PD3);
  st_enable(false);

  //Timer 2 is configuring
  TCCR2A = 0;
  //Set CTC mode
  TCCR2A |= (1 << WGM21);
  //Set prescaler 1
  TCCR2B |= (1 << CS20);
  OCR2A = 5;

  sei();
}

void st_set_stepper(Stepper *st)
{
  stepper = st;
  m_accel_factor = stepper->accel / ((long double)TIMER_FREQUENCY * TIMER_FREQUENCY);
}

// void st_calculate()
// {
//   if (!calc_req)
//     return;
//   if (st_plan.steps_remaining > st_plan.accel_point)
//   {
//     // q = m_accel_factor * m_period * m_period;
//     // m_period = m_period * (1 - (q + q * q));
//     m_period = m_period * (1 - m_accel_factor * m_period * m_period);
//   }
//   else if (st_plan.steps_remaining < st_plan.decel_point)
//   {
//     // q = m_accel_factor * m_period * m_period;
//     // m_period = m_period * (1 + (q + q * q));
//     m_period = m_period * (1 + m_accel_factor * m_period * m_period);
//   }
//   else
//   {
//     if (st_frequency() < speed)
//     {
//       m_period = m_period * (1 - m_accel_factor * m_period * m_period);
//     }
//   }
//   calc_req = false;
// }

void st_calculate()
{
  //If calculation flag is not set
  if (!calc_req)
    return;

  if(m_period < 40)
    return;

  float period = m_period;
  //If there are steps for enough to decelerate then we can accelerate
  //If target speed(period) is still lower than current then accelerate
  if (st_plan.decel_point < st_plan.steps_remaining && st_plan.target_period < m_period)
  {
    //Calculate period
    m_period = period * (1 - m_accel_factor * period * period);
    //Decel point should be equal to accelerated steps
    //In other words, we should have decelerating steps equal to accelerating steps
    st_plan.decel_point++;
    //If target_period is so close to current period, then just assign it
    if(st_plan.target_period > m_period){
      m_period = st_plan.target_period;
    }

    bitSet(State , RAMPING_BIT);
  }
  else if (st_plan.decel_point < st_plan.steps_remaining && st_plan.target_period > m_period)
  {
    //Calculate period
    m_period = period * (1 + m_accel_factor * period * period);
    //Therotically, it never goes below zero, but it is better to check
    if (st_plan.decel_point > 0)
      st_plan.decel_point--;
    //If target_period is so close to current period, then just assign it
    if(st_plan.target_period < m_period){
      m_period = st_plan.target_period;
    }

    bitSet(State , RAMPING_BIT);
  }
  else if (st_plan.steps_remaining <= st_plan.decel_point)
  {
    m_period = period * (1 + m_accel_factor * period * period);
    bitSet(State , RAMPING_BIT);
  } else {
    bitClear(State , RAMPING_BIT);
  }

  calc_req = false;
}

void st_decelerate()
{
  //Is moving
  //And if accelerating or cruise bit
  if ((State & MOVING_MASK) && (State & (ACCELERATING_MASK | CRUISE_MASK)))
  {
    st_plan.steps_remaining -= st_plan.steps_remaining - st_plan.decel_point;
    st_plan.decel_point = st_plan.steps_remaining;
  }
}

void st_hard_stop()
{
  st_plan.steps_remaining = 0;
}

void st_move(int32_t pos, int32_t speed)
{
  //Calculate how many steps are required to move to the pos
  st_plan.steps_remaining = abs(pos - (stepper->posInSteps));
  st_plan.steps_total = st_plan.steps_remaining;
  st_plan.target_period = TIMER_FREQUENCY / (float)speed;
  st_plan.decel_point = 0;
  m_period = TIMER_FREQUENCY / sqrtf(2 * stepper->accel);

  if (pos - stepper->posInSteps > 0)
  {
    State |= DIRECTION_MASK;
    PORTD |= DIR_MASK;
  }
  else
  {
    State &= ~DIRECTION_MASK;
    PORTD &= ~(DIR_MASK);
  }

  // State |= MOVING_MASK | ACCELERATING_MASK;
  bitSet(State , MOVING_BIT);
  bitSet(State , ACCELERATING_BIT);

  //Enable stepper if not
  st_enable(true);

  TCCR1A = 0;
  TCCR1B = 0;
  //Assign stepper timer
  TCCR1B |= _BV(WGM12) | _BV(CS11);
  TIMSK1 = 0;
  TIMSK2 = 0;
  OCR1A = m_period;
  TIMSK1 |= _BV(OCIE1A);
}

long st_frequency()
{
  //If it is not moving, directly return 0
  if (!(State & MOVING_MASK))
    return 0;

  //Calculate and return
  return TIMER_FREQUENCY / m_period;
}

ISR(TIMER1_COMPA_vect)
{
  if (st_plan.steps_remaining == 0)
  {
    TIMSK1 &= ~(1 << OCIE1A);
    State &= ~MOVING_MASK;
    m_period = 0;
    return;
  }
  else
  {
    OCR1A = round(m_period);
  }

  //If stop is enabled, checking it
  if (currentSettings.stopOnLimit)
  {
    //Check whether on limit
    if (onLimit())
    {
      //Stop it
      st_stop(false);
      TIMSK1 &= ~(1 << OCIE1A); //Disable timer
      State &= ~MOVING_MASK; //Recover state
      m_period = 0; //Reset period
    }
  }

  //If rotating cw direction, then increment steps position
  if (State & DIRECTION_MASK)
  {
    stepper->posInSteps++;
  }
  else
  { //If rotating ccw direction, then decrement steps position
    stepper->posInSteps--;
  }

  PORTD |= PULSE_MASK;
  TIMSK2 |= (1 << OCIE2A);
  st_plan.steps_remaining--;
  calc_req = true;
}

ISR(TIMER2_COMPA_vect)
{
  //Disable low edge timer
  TIMSK2 &= ~(1 << OCIE2A);
  //Apply opposite of pulsemask
  PORTD &= ~(stepper->pulseMask);

  if(st_plan.steps_remaining <= st_plan.decel_point)
    st_calculate();
  //Calculate next pulse period
  // st_calculate();
}

void st_stop(bool soft)
{
  if (soft && st_plan.steps_remaining > st_plan.decel_point)
  {
    st_plan.steps_remaining = st_plan.decel_point;
  }
  else
  {
    st_plan.steps_remaining = 0;
  }
}

void st_enable(bool enabled)
{
  //Apply mask
  if (enabled)
    PORTD &= ~ENABLE_MASK;
  else
    PORTD |= ENABLE_MASK;
}

void st_pulse(){
  if (st_plan.steps_remaining == 0)
  {
    TIMSK1 &= ~(1 << OCIE1A);
    State &= ~MOVING_MASK;
    m_period = 0;
    return;
  }
  else
  {
    OCR1A = (unsigned int)m_period;
  }

  //If stop is enabled, checking it
  if (currentSettings.stopOnLimit)
  {
    //Check whether on limit
    if (onLimit())
    {
      //Stop it
      st_stop(false);
      TIMSK1 &= ~(1 << OCIE1A); //Disable timer
      State &= ~MOVING_MASK; //Recover state
      m_period = 0; //Reset period
    }
  }

  //If rotating cw direction, then increment steps position
  if (State & DIRECTION_MASK)
  {
    stepper->posInSteps++;
  }
  else
  { //If rotating ccw direction, then decrement steps position
    stepper->posInSteps--;
  }

  PORTD |= PULSE_MASK;
  TIMSK2 |= (1 << OCIE2A);
  st_plan.steps_remaining--;
  calc_req = true;
}