#ifndef STEPPER_DRIVER_H
#define STEPPER_DRIVER_H

#include <stdint.h>

// #define TIMER_FREQUENCY 2000000UL
// #define TIMER_FREQUENCY 1991000UL
#define TIMER_FREQUENCY 1993000UL


#define MOVING_BIT          0
#define ACCELERATING_BIT    1
#define DECELERATING_BIT    2
#define CRUISE_BIT          3
#define DIRECTION_BIT       4
#define RAMPING_BIT         5

#define MOVING_MASK          (1 << MOVING_BIT)
#define ACCELERATING_MASK    (1 << ACCELERATING_BIT)
#define DECELERATING_MASK    (1 << DECELERATING_BIT)
#define CRUISE_MASK          (1 << CRUISE_BIT)
#define DIRECTION_MASK       (1 << DIRECTION_BIT)

#define PULSE_PIN   PD2
#define DIR_PIN     PD3
#define ENABLE_PIN  PD7

#define PULSE_MASK (1 << PD2)
#define DIR_MASK (1 << PD3)
#define ENABLE_MASK (1 << PD7)

class Stepper;

typedef struct {
    unsigned long steps_total;
    volatile unsigned long steps_remaining;
    volatile float target_period;
    unsigned long steps_half;
    unsigned long accel_point;
    volatile unsigned long decel_point;
} plan_t;

void     st_init();
void     st_move(int32_t pos , int32_t speed);
void     st_decelerate();
void     st_hard_stop();
void     st_calculate();
void     st_set_stepper(Stepper* stepper);
void     st_stop(bool soft);
void     st_rising_handler();
void     st_falling_handler();
long     st_frequency();
void     st_enable(bool enabled);
void     st_pulse();

#endif