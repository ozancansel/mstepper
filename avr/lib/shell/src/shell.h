#ifndef SHELL_H
#define SHELL_H

#include <stdint.h>

#ifndef BUFFER_SIZE
#define BUFFER_SIZE 128
#endif

class IStream
{
  public:
    virtual int available() = 0;
    virtual int read() = 0;
};

class Shell
{
  public:
    Shell(IStream *stream , void(*callback)(char** , int));
    void listen();
    void reset();
    void feed(int b);
  private:
    IStream *m_stream;
    char    m_buffer[BUFFER_SIZE];
    int     m_idx;
    void(*m_cb)(char** , int);
};

#endif