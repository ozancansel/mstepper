#include "parser.h"
#include <string.h>
#include <Arduino.h>

Parser::Parser(char** cmd , int length)
:
    m_cmd(cmd) ,
    m_idx(0) ,
    m_length(length)
{   }

char* Parser::getNext(){
    if(!hasNext())
        return NULL;
    m_idx++;
    return m_cmd[m_idx];
}

bool Parser::hasNext(){
    return m_idx < (m_length - 1);
}

char* Parser::cmd(){
    return m_cmd[0];
}

bool Parser::isOption(){

    if(strlen(current()) < 1)
        return false;

    int length = strlen(current());
    if(length > 1){
        if(current()[0] != '-')
            return false;
        
        if(current()[1] == '-' && length > 2 && current()[2] > 65)
            return true;
        
        //If is not digit
        if(current()[1] < 65)
            return false;
    }
    else
        return false;
}

bool Parser::exist(char* option){
    for(int i = 0; i < m_length; i++){
        if(strcmp(m_cmd[i] , option) == 0)
            return true;
    }

    return false;
}

bool Parser::existP(char* option)
{

    for(int i = 0; i < m_length; i++){
        if(strcmp_P(m_cmd[i] , option) == 0)
            return true;
    }

    return false;
}

bool Parser::equal(const char* str){
    return strcmp(m_cmd[m_idx] , str) == 0;
}

bool Parser::equalP(const char* str){
    return strcmp_P(current() , str) == 0;
}

bool Parser::equalP(const char* first , const char* strP){
    return strcmp_P(first , strP) == 0;
}

char* Parser::current(){
    return m_cmd[m_idx];
}

char* Parser::peek(){

    if(hasNext()){
        return m_cmd[m_idx + 1];
    }

    return nullptr;
}

void Parser::reset(){
    m_idx = 0;
}