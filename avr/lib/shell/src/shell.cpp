#include "shell.h"
#include <string.h>
#include <stdlib.h>
#include <Arduino.h>
#include <MemoryFree.h>

Shell::Shell(IStream *stream, void (*callback)(char **, int))
    : m_stream(stream),
      m_cb(callback)
{
    reset();
}

void Shell::listen()
{
    //If there is any available bytes
    while (m_stream->available())
    {
        int byte = m_stream->read();

        feed(byte);
    }
}

void Shell::feed(int byte)
{
    //If command is ended
    if (byte == '\n' || byte == '\r')
    {

        if (m_idx == 0)
            return;

        //Terminate buffer
        m_buffer[m_idx] = '\0';

        char *dup , *orig;
        char *token;
        char* args[12];
        int length = 0;

        // args = (char **)malloc(sizeof(char **) * 12);
        // args = new char*[12];

        orig = dup = strdup(m_buffer);
        const char delimiters[] = " ";

        while (1)
        {
            // args = (char**)realloc(args , sizeof(char**) * length + 1);
            token = strsep(&dup, delimiters);
            if (token == NULL)
                break;

            args[length] = token;
            length++;
            if (length >= 12)
                break;
        }

        //Trigger callback function
        m_cb(args, length);

        // delete args;
        delete orig;

        //Reset buffer
        reset();
    }
    else
    {
        //Command is receiving
        m_buffer[m_idx] = byte;
        m_idx++;
    }
}

void Shell::reset()
{
    m_idx = 0;
}