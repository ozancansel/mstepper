#include "limit.h"
#include "settings.h"

extern Settings settings;

bool onLimit(){
    return settings.endLogicTrue == 0 ? bit_is_clear(PINB, PB0) : bit_is_set(PINB, PB0);
}