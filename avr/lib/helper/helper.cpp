#include "helper.h"

void printBits(uint8_t myByte)
{
    for (uint8_t mask = 0x80; mask; mask >>= 1)
    {
        if (mask & myByte)
            Serial.print('1');
        else
            Serial.print('0');
    }
}