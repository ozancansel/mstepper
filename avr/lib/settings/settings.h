#ifndef SETTINGS_H
#define SETTINGS_H

#include <Arduino.h>
#define ALGORITHM_1     1
#define MODE_POSITION   1

struct Settings{
    long     acceleration;
    long     mode;
    long     algorithm;
    long     i2cAddress;
    long     microstepping;
    long     referenceForwardSpeed;
    long     referenceBackwardSpeed;
    long     referenceBackwardVector;
    long     referenceAfterPos;
    long     endLogicTrue;
    long     referenceOnStart;
    long     referenceMoveDirection;
    uint8_t  stopOnLimit;
};

void    loadSettings();
void    saveSettings();
void    writeByte(int addr , uint8_t b);
uint8_t readByte(int addr);
long    readLong(long addr);
void    writeLong(long addr, long num);

extern Settings settings;

#endif