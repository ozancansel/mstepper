#include "settings.h"
#include <EEPROM.h>

Settings settings;
Settings currentSettings;

void loadSettings()
{
    //Read settings from eeprom
    settings.acceleration = readLong(0);
    settings.algorithm = readLong(4);
    settings.i2cAddress = readLong(8);
    settings.mode = readLong(12);
    settings.microstepping = readLong(16);
    settings.referenceForwardSpeed = readLong(20);
    settings.referenceBackwardSpeed = readLong(24);
    settings.referenceBackwardVector = readLong(28);
    settings.referenceAfterPos = readLong(32);
    settings.endLogicTrue = readLong(36);
    settings.referenceOnStart = readLong(40);
    settings.referenceMoveDirection = readLong(44);
    settings.stopOnLimit = readByte(49);
    currentSettings = settings;
}

void saveSettings()
{
    //All values are checking before saving to reduce write/read to eeprom
    
    if(readLong(0) != settings.acceleration)
        writeLong(0 , settings.acceleration);
    if(readLong(4) != settings.algorithm)
        writeLong(4 , settings.algorithm);
    if(readLong(8) != settings.i2cAddress)
        writeLong(8, settings.i2cAddress);
    if(readLong(12 != settings.mode))
        writeLong(12 , settings.mode);
    if(readLong(16) != settings.microstepping)
        writeLong(16 , settings.microstepping);
    if(readLong(20) != settings.referenceForwardSpeed)
        writeLong(20 , settings.referenceForwardSpeed);
    if(readLong(24) != settings.referenceBackwardSpeed)
        writeLong(24 , settings.referenceBackwardSpeed);
    if(readLong(28) != settings.referenceBackwardVector)
        writeLong(28 , settings.referenceBackwardVector);
    if(readLong(32) != settings.referenceAfterPos)
        writeLong(32 , settings.referenceAfterPos);
    if(readLong(36) != settings.endLogicTrue)
        writeLong(36 , settings.endLogicTrue);
    if(readLong(40) != settings.referenceOnStart)
        writeLong(40 , settings.referenceOnStart);
    if(readLong(44) != settings.referenceMoveDirection)
        writeLong(44 , settings.referenceMoveDirection);
    if(readByte(49) != settings.stopOnLimit)
        writeByte(49 , settings.stopOnLimit);
}

long readLong(long addr)
{
    union {
        byte b[4];
        long num;
    } data;

    for (int i = 0; i < 4; i++)
    {
        data.b[i] = EEPROM.read(addr + i);
    }

    return data.num;
}

void writeLong(long addr, long num)
{
    union {
        byte b[4];
        long num;
    } data;

    data.num = num;

    for (int i = 0; i < 4; i++)
    {
        EEPROM.write(addr + i , data.b[i]);
    }
}

uint8_t readByte(int addr){
    return EEPROM.read(addr);
}

void writeByte(int addr, uint8_t b){
    EEPROM.write(addr , b);
}