#include <Arduino.h>
#include <stepper.h>
#include <stepperdriver.h>
#include <shell.h>
#include <string.h>
#include <stdlib.h>
#include <parser.h>
#include <settings.h>
#include <Wire.h>
#include <MemoryFree.h>
#include <avr/wdt.h>
#include <limit.h>
#include <helper.h>

#define END_SWITCH_INPUT 8
#define HOST_SERIAL Serial
#define ERR_POS_NOT_SPECIFIED 2
#define ERR_SPEED_NOT_SPECIFIED 3
#define ERR_VALUE_SHOULD_BE_POSITIVE 4
#define ERR_ACCEL_NOT_SPECIFIED 5
#define ERR_VALUE_NOT_SPECIFIED 6
#define ERR_MICROSTEP_OUTBOUNDARY 7
#define ERR_INVALID_I2C_ADDR 8
#define ERR_REFERENCE_PULL_OFF 9
#define ERR_INVALID_VALUE 10
#define ERR_MOTOR_IS_NOT_MOVING 11
#define ERR_MOVING 12
#define ERR_NUMBER_PARSE_ERR 13
#define ERR_CURRENTLY_REFERENCING 14
#define ERR_OPTION_NOT_SPECIFIED 15

#define OPTS_SYNC (1 << 0)
#define OPTS_REST (1 << 1)

#define MS1 PD4
#define MS2 PD5
#define MS3 PD6

#define OK_MESSAGE ';'
#define REFERENCING_BIT 0
#define REFERENCING_MASK (1 << REFERENCING_BIT)

class UartStream : public IStream
{
  public:
    UartStream(HardwareSerial *serial) : m_serial(serial) {}
    int available() { return m_serial->available(); }
    int read() { return m_serial->read(); }

  private:
    HardwareSerial *m_serial;
};

int microstepTable[] = {
    0,                                   // 0 0 0
    (1 << MS1),                          // 1 0 0
    (1 << MS2),                          // 0 1 0
    (1 << MS1) | (1 << MS2),             // 1 1 0
    (1 << MS3) ,                         // 0 0 1
    (1 << MS1) | (1 << MS2) | (1 << MS3) // 1 1 1
};

int programState = 0;

void reset();
void resetHard();
bool reference();
void stop(bool soft);
void applyMicrostepping(int microstep);
void onReceiveEvent(int bytes);
void onRequestEvent();
void waitForMotionEnd();
void waitForRampingEnd();
bool checkTempOption(Parser *p);
bool checkOption(const char* strP , Parser* p);
void test();

void jog(long magnitude, long speed, long opts = 0);

Stepper st;

extern plan_t st_plan;
extern int State;
extern Settings settings;
extern Settings currentSettings;
extern bool calc_req;

long calculated = 0;
bool restAfter = false;

void sendOkMessage();
void sendErrMessage(int code);
void onCommandReceived(char **cmd, int length);
void onI2cCommandReceived(char **cmd, int length);

Shell sh(new UartStream(&Serial), onCommandReceived);
Shell sh2(new UartStream(&Serial), onCommandReceived);

void setup()
{

    DDRD |= (1 << MS1) | (1 << MS2) | (1 << MS3);
    st_init();

    Serial.begin(9600);
    loadSettings();

    //Reset
    reset();

    // test();
    // while(1) { }

    if (currentSettings.referenceOnStart)
    {
        programState |= REFERENCING_MASK;
        reference();
        delay(100);
        reference();
        programState &= ~REFERENCING_MASK;
    }
}

void loop()
{
    sh.listen();
    st_calculate();

    //If moving ended
    //and
    //restAfter flag is set
    if (bit_is_clear(State, 0) && restAfter)
    {
        //Disable stepper
        st_enable(false);
        restAfter = false; //Clear restAfter flag
    }

    //If reference command is received
    if (bit_is_set(programState, REFERENCING_BIT))
    {
        //Refernce and
        if (!reference()) //check result
        {                 //if unsuccessfull
            //Send error message
            sendErrMessage(ERR_REFERENCE_PULL_OFF); //pull off err
            programState &= ~REFERENCING_MASK;      //Recover program state
            bitClear(programState, REFERENCING_BIT);
            return; //Return
        }
        delay(120);
        if (!reference())
        {
            sendErrMessage(ERR_REFERENCE_PULL_OFF); //pull off error
            programState &= ~REFERENCING_MASK;      // Recover program state
            bitClear(programState, REFERENCING_BIT);
            return;
        }
        else
        {
            sendOkMessage(); //ok
        }
        //Recover program state
        bitClear(programState, REFERENCING_BIT);
    }
}

void onCommandReceived(char **cmd, int length)
{

    Parser p(cmd, length);

    if (p.equalP(PSTR("move")))
    {
        //If it is already moving
        if (bit_is_set(State, MOVING_BIT))
        {
            //Return error code
            sendErrMessage(ERR_MOVING);
            return;
        }

        //A flag which refers to sync options
        bool sync = false;
        bool rest = false;

        //If position not specified
        if (!p.hasNext())
        {
            //Return position not specified
            sendErrMessage(ERR_POS_NOT_SPECIFIED);
            return;
        }

        //Retrieve next word
        while (p.hasNext())
        {
            p.getNext();

            //If any option is specified
            if (p.isOption())
            {
                //If it is --sync option
                if (p.equalP(PSTR("--sync")) || p.equalP(PSTR("-s")))
                    sync = true; //Set sync flag true
                else if (p.equalP(PSTR("--rest")) || p.equalP(PSTR("-r")))
                    rest = true;
            }
            else
            {
                break;
            }
        }

        char *endptr;
        long pos = strtol(p.current(), &endptr, 10);
        //If number could not be parsed
        if (*endptr != '\0')
        {
            //Send corresponding error code
            sendErrMessage(ERR_NUMBER_PARSE_ERR);
            return; //Don't resume, return
        }

        if (!p.hasNext())
        {
            sendErrMessage(ERR_SPEED_NOT_SPECIFIED);
            return;
        }

        long speed = strtol(p.getNext(), &endptr, 10);
        if (*endptr != '\0')
        {
            //Send corresponding error code
            sendErrMessage(ERR_NUMBER_PARSE_ERR);
            return; //Don't resume, return
        }

        //Validate speed
        if (speed <= 0)
        {
            sendErrMessage(ERR_VALUE_SHOULD_BE_POSITIVE);
            return;
        }

        //Move to position with the speed
        st_move(pos, speed);

        //If rest flag is set
        if (rest)
            restAfter = rest; //Apply res flag to program

        //If sync option is specified
        if (sync)
        {
            //Wait for motion end
            waitForMotionEnd();
        }
        sendOkMessage();
    }
    else if (p.equalP(PSTR("jog")))
    {
        if (programState & REFERENCING_MASK)
            return;

        bool sync = false; //Sync flag
        bool rest = false; //Rest flag

        //If position not specified
        if (!p.hasNext())
        {
            sendErrMessage(ERR_POS_NOT_SPECIFIED);
            return;
        }

        while (p.hasNext())
        {
            //Retrieve option
            p.getNext();

            //Check whether it is option
            if (p.isOption())
            {
                //If --sync option is specified
                if (p.equalP(PSTR("--sync")) || p.equalP(PSTR("-s")))
                    sync = true; //set sync flag true
                else if (p.equalP(PSTR("--rest")) || p.equalP(PSTR("-r")))
                {
                    rest = true; //set rest flag true
                }
            }
            else
            {
                break; //If value is not option, break
            }
        }

        char *endptr;                                   //used in strtol to check whether it is fully parsed
        long vector = strtol(p.current(), &endptr, 10); //p.current() used because in while loop it is retrieved

        //If number could not be parsed
        if (*endptr != '\0')
        {
            //Send corresponding error code
            sendErrMessage(ERR_NUMBER_PARSE_ERR);
            return; //Don't resume, return
        }

        //Check whether speed is specified
        if (!p.hasNext())
        {
            sendErrMessage(ERR_SPEED_NOT_SPECIFIED);
            return;
        }

        //Retrieve speed
        p.getNext();
        //Convert speed string to long
        long speed = strtol(p.current(), &endptr, 10);
        //If number could not be parsed
        if (*endptr != '\0')
        {
            //Send corresponding error code
            sendErrMessage(ERR_NUMBER_PARSE_ERR);
            return; //Don't resume, return
        }

        //Validate speed
        //If speed is non-positive
        if (speed <= 0)
        {
            sendErrMessage(ERR_VALUE_SHOULD_BE_POSITIVE); //Send error
            return;                                       //Dont resume , return
        }

        int opts = 0;

        if (sync)
            opts |= OPTS_SYNC;
        if (rest)
            restAfter = rest;

        jog(vector, speed, opts);

        sendOkMessage();
    }
    else if (p.equalP(PSTR("pos")))
    {
        HOST_SERIAL.print("pos ");
        HOST_SERIAL.println(st.posInSteps);
    }
    else if (p.equalP(PSTR("set")))
    {
        //cannot change anytinh while referencing
        if (programState & REFERENCING_MASK)
        {
            sendErrMessage(ERR_CURRENTLY_REFERENCING); //Return err code
            return;                                    //Dont resume, return
        }

        //If there is no option
        if (!p.hasNext())
        {
            //Send error message
            sendErrMessage(ERR_OPTION_NOT_SPECIFIED);
            return; //dont resume, return
        }

        //Iterate over commands
        while (p.hasNext())
        {
            //Iterate
            p.getNext();
            if (!p.isOption())
                continue;

            //If option is --accel
            if (p.equalP(PSTR("--accel")) || p.equalP(PSTR("-a")))
            {
                //Acceleration cannot be changed while motor is moving
                if (bit_is_set(programState , REFERENCING_BIT) || bit_is_set(State , MOVING_BIT))
                {
                    sendErrMessage(ERR_MOVING); //Return error code
                    continue;                   //Ignore this option
                }

                //Holds temporary flag
                bool temporary = checkTempOption(&p); //Check temp option

                //If accel option is not specified
                if (!p.hasNext())
                {
                    sendErrMessage(ERR_VALUE_NOT_SPECIFIED);
                    continue;
                }

                char *endptr;
                long accel = strtol(p.getNext(), &endptr, 10);
                //If number could not be parsed
                if (*endptr != '\0')
                {
                    //Send corresponding error code
                    sendErrMessage(ERR_NUMBER_PARSE_ERR);
                    return; //Don't resume, return
                }

                if (accel <= 0)
                {
                    sendErrMessage(ERR_VALUE_SHOULD_BE_POSITIVE);
                    continue;
                }

                //Set acceleration
                currentSettings.acceleration = accel;
                st.setAccel(accel);
                st_set_stepper(&st);

                //If temp flag is set false
                if (!temporary)
                {
                    //Change acceleration
                    settings.acceleration = currentSettings.acceleration;
                    //Make settings permanent
                    saveSettings();
                }

                sendOkMessage();
            }
            else if (p.equalP(PSTR("--pos")) || p.equalP(PSTR("-p")))
            {

                if (!p.hasNext())
                {
                    sendErrMessage(ERR_VALUE_NOT_SPECIFIED);
                    continue;
                }

                char *endptr;
                long val = strtol(p.getNext(), &endptr, 10);
                //If number could not be parsed
                if (*endptr != '\0')
                {
                    //Send corresponding error code
                    sendErrMessage(ERR_NUMBER_PARSE_ERR);
                    continue; //Don't resume, return
                }
                //Set position
                st.posInSteps = val;

                sendOkMessage();
            }
            else if (p.equalP(PSTR("--microstepping")) || p.equalP(PSTR("-m")))
            {
                //It is not permitted to change microstepping while motor is moving
                if (State & MOVING_MASK)
                {
                    //Send error code
                    sendErrMessage(ERR_MOVING);
                    return;
                }

                //Holds temporary flag
                bool temporary = checkTempOption(&p); //Check temp option

                //If microstepping value or
                if (!p.hasNext())
                {
                    sendErrMessage(ERR_VALUE_NOT_SPECIFIED);
                    continue;
                }

                char *endptr;
                long microstep = strtol(p.getNext(), &endptr, 10);
                //If number could not be parsed
                if (*endptr != '\0')
                {
                    //Send corresponding error code
                    sendErrMessage(ERR_NUMBER_PARSE_ERR);
                    continue; //Don't resume, return
                }

                if (microstep < 0 || microstep > 4)
                {
                    sendErrMessage(ERR_MICROSTEP_OUTBOUNDARY);
                    continue;
                }

                currentSettings.microstepping = microstep;
                applyMicrostepping(currentSettings.microstepping);

                if (!temporary)
                {
                    //Set microstepping
                    settings.microstepping = microstep;
                    //Save settings to EEPROM
                    saveSettings();
                }

                //Send success message
                sendOkMessage();
            }
            else if (p.equalP(PSTR("--i2cAddr")) || p.equalP(PSTR("-i")))
            {

                if (!p.hasNext())
                {
                    sendErrMessage(ERR_VALUE_NOT_SPECIFIED);
                    continue;
                }

                char *endptr;
                long addr = strtol(p.getNext(), &endptr, 10);
                //If number could not be parsed
                if (*endptr != '\0')
                {
                    //Send corresponding error code
                    sendErrMessage(ERR_NUMBER_PARSE_ERR);
                    continue; //Don't resume, return
                }
                // int addr = atoi(p.getNext());

                //If address is outbound
                if (addr < 0 || addr > 127)
                {
                    //Return error
                    sendErrMessage(ERR_INVALID_VALUE);
                    continue;
                }
                
                currentSettings.i2cAddress = addr;
                settings.i2cAddress = addr;

                //Saving settings
                saveSettings();

                sendOkMessage();
            }
            else if (p.equalP(PSTR("--refForwardS")) || p.equalP(PSTR("-rfs")))
            {

                //Holds temporary flag
                bool temporary = checkTempOption(&p); //Check temp option

                if (!p.hasNext())
                {
                    sendErrMessage(ERR_VALUE_NOT_SPECIFIED);
                    continue;
                }

                char *endptr;
                long forwardSpeed = strtol(p.getNext(), &endptr, 10);

                //If number could not be parsed
                if (*endptr != '\0')
                {
                    //Send corresponding error code
                    sendErrMessage(ERR_NUMBER_PARSE_ERR);
                    continue; //Don't resume, return
                }
                // int addr = atoi(p.getNext());

                if (forwardSpeed <= 0)
                {
                    sendErrMessage(ERR_INVALID_VALUE);
                    continue;
                }

                currentSettings.referenceForwardSpeed = forwardSpeed;

                //If temporary option is not specified
                //saving settings
                if (!temporary)
                {
                    settings.referenceForwardSpeed = currentSettings.referenceForwardSpeed;
                    saveSettings();
                }

                sendOkMessage();
            }
            else if (p.equalP(PSTR("--refBackwardS")) || p.equalP(PSTR("-rbs")))
            {

                //Holds temporary flag
                bool temporary = checkTempOption(&p); //Check temp option

                if (!p.hasNext())
                {
                    sendErrMessage(ERR_VALUE_NOT_SPECIFIED);
                    continue;
                }

                // long speed = atoi(p.getNext());
                char *endptr;
                long speed = strtol(p.getNext(), &endptr, 10);

                //If number could not be parsed
                if (*endptr != '\0')
                {
                    //Send corresponding error code
                    sendErrMessage(ERR_NUMBER_PARSE_ERR);
                    continue; //Don't resume, return
                }

                if (speed <= 0)
                {
                    sendErrMessage(ERR_INVALID_VALUE);
                    continue;
                }

                currentSettings.referenceBackwardSpeed = speed;

                //If settings is not temporary
                if (!temporary)
                {
                    settings.referenceBackwardSpeed = speed; //Assign storage settings
                    saveSettings();                          //Save settings permanently
                }

                sendOkMessage();
            }
            else if (p.equalP(PSTR("--refBackwardV")) || p.equalP(PSTR("-rbv")))
            {
                //Holds temporary flag
                bool temporary = checkTempOption(&p); //Check temp option

                if (!p.hasNext())
                {
                    sendErrMessage(ERR_VALUE_NOT_SPECIFIED);
                    continue;
                }

                char *endptr;
                long speed = strtol(p.getNext(), &endptr, 10);
                //If number could not be parsed
                if (*endptr != '\0')
                {
                    //Send corresponding error code
                    sendErrMessage(ERR_NUMBER_PARSE_ERR);
                    continue; //Don't resume, return
                }

                //Speed could not be zero or negative
                if (speed <= 0)
                {
                    sendErrMessage(ERR_INVALID_VALUE);
                    continue;
                }

                currentSettings.referenceBackwardVector = speed;

                //If setting is permanent
                if(!temporary){
                    //Assign value from temporary settings to permanent settings
                    settings.referenceBackwardVector = currentSettings.referenceBackwardVector;
                    saveSettings(); //Save settings
                }

                sendOkMessage();
            }
            else if (p.equalP(PSTR("--refAfterPos")) || p.equalP(PSTR("-rap")))
            {
                //Holds temporary flag
                bool temporary = checkTempOption(&p); //Check temp option

                if (!p.hasNext())
                {
                    sendErrMessage(ERR_VALUE_NOT_SPECIFIED);
                    continue;
                }

                char *endptr;
                long pos = strtol(p.getNext(), &endptr, 10);
                //If number could not be parsed
                if (*endptr != '\0')
                {
                    //Send corresponding error code
                    sendErrMessage(ERR_NUMBER_PARSE_ERR);
                    continue; //Don't resume, return
                }

                currentSettings.referenceAfterPos = pos;

                //If settings is permanent
                if(!temporary){
                    //Assign value from temporary settings to permanent settings
                    settings.referenceAfterPos = currentSettings.referenceAfterPos;
                    saveSettings(); //Make permanent
                }

                sendOkMessage();
            }
            else if (p.equalP(PSTR("--endLogicTrue")) || p.equalP(PSTR("-elt")))
            {
                //Holds temporary flag
                bool temporary = checkTempOption(&p); //Check temp option

                if (!p.hasNext())
                {
                    sendErrMessage(ERR_VALUE_NOT_SPECIFIED);
                    continue;
                }

                char *endptr;
                long engLogicVal = strtol(p.getNext(), &endptr, 10);
                //If number could not be parsed
                if (*endptr != '\0')
                {
                    //Send corresponding error code
                    sendErrMessage(ERR_NUMBER_PARSE_ERR);
                    continue; //Don't resume, return
                }

                if (engLogicVal != 1 && engLogicVal != 0)
                {
                    sendErrMessage(ERR_INVALID_VALUE);
                    continue;
                }

                currentSettings.endLogicTrue = engLogicVal;

                //If settings is permanent
                if(!temporary){
                    //Assign value from temporary settings to permanent settings
                    settings.endLogicTrue = currentSettings.endLogicTrue;
                    saveSettings(); //Make permanent
                }

                sendOkMessage();
            }
            else if (p.equalP(PSTR("--refOnStart")) || p.equalP(PSTR("-ros")))
            {
                
                if (!p.hasNext())
                {
                    sendErrMessage(ERR_VALUE_NOT_SPECIFIED);
                    continue;
                }

                char *endptr;
                long enabled = strtol(p.getNext(), &endptr, 10);
                //If number could not be parsed
                if (*endptr != '\0')
                {
                    //Send corresponding error code
                    sendErrMessage(ERR_NUMBER_PARSE_ERR);
                    continue; //Don't resume, return
                }
                
                //Value should be zero or negative
                //If not
                if (enabled < 0)
                {
                    sendErrMessage(ERR_INVALID_VALUE); //send error
                    continue;
                }

                //Assign value to corresponding setting
                currentSettings.referenceOnStart = enabled;
                settings.referenceOnStart = enabled;
                saveSettings(); //Make permanent

                sendOkMessage();
            }
            else if (p.equalP(PSTR("--refMoveDir")) || p.equalP(PSTR("-rmd")))
            {
                
                //Holds temporary flag
                bool temporary = checkTempOption(&p); //Check temp option

                if (!p.hasNext())
                {
                    sendErrMessage(ERR_VALUE_NOT_SPECIFIED);
                    continue;
                }

                char *endptr;
                long dir = strtol(p.getNext(), &endptr, 10);
                //If number could not be parsed
                if (*endptr != '\0')
                {
                    //Send corresponding error code
                    sendErrMessage(ERR_NUMBER_PARSE_ERR);
                    continue; //Don't resume, return
                }

                if (dir != 1 && dir != -1)
                {
                    sendErrMessage(ERR_INVALID_VALUE);
                    return;
                }

                currentSettings.referenceMoveDirection = dir;

                //If it is stated that the setting is permanent
                if(!temporary){
                    //Assign temporary settings value to permanent one
                    settings.referenceMoveDirection = currentSettings.referenceMoveDirection;
                    saveSettings(); //Make permanent
                }

                sendOkMessage();
            }
            else if (p.equalP(PSTR("--stopOnLimit")) || p.equalP(PSTR("-sol")))
            {
                
                //Holds temporary flag
                bool temporary = checkTempOption(&p); //Check temp option

                //If value is not specified
                if (!p.hasNext())
                {
                    sendErrMessage(ERR_VALUE_NOT_SPECIFIED);
                    continue;
                }

                char *endptr;
                long enabled = strtol(p.getNext(), &endptr, 10);
                //If number could not be parsed
                if (*endptr != '\0')
                {
                    //Send corresponding error code
                    sendErrMessage(ERR_NUMBER_PARSE_ERR);
                    continue; //Don't resume, return
                }

                //If value is not 0 or 1, it means it is invalid
                if (enabled != 0 && enabled != 1)
                {
                    //Send erro code
                    sendErrMessage(ERR_INVALID_VALUE);
                    continue;
                }

                currentSettings.stopOnLimit = (uint8_t)enabled;

                //If it is stated that the setting is permanent
                if(!temporary){
                    //Assign temporary settings' value to permanent one
                    settings.stopOnLimit = currentSettings.stopOnLimit;
                    saveSettings(); //Make permanent
                }

                //Respond with success
                sendOkMessage();
            }
        }
    }
    else if (p.equalP(PSTR("running")))
    {
        if (bit_is_set(State, MOVING_BIT) || bit_is_set(programState, REFERENCING_BIT))
            HOST_SERIAL.println(F("yes"));
        else
            HOST_SERIAL.println(F("no"));
    }
    else if (p.equalP(PSTR("stop")))
    {

        if (p.hasNext())
        {

            p.getNext();
            bool soft = false;
            bool sync = false;

            //If motor is moving and soft stop is specified
            if (p.existP(PSTR("--soft")) || p.existP(PSTR("-so")))
                soft = true;
            if (p.existP(PSTR("--sync")) || p.existP(PSTR("-s")))
                sync = true;

            //If soft option is not specified
            if (!soft)
                stop(false);
            else
            {
                stop(true);

                if (sync)
                {
                    waitForMotionEnd();
                }
            }

            //Send ok message
            sendOkMessage();
        }
        else
        {
            stop(false);
            sendOkMessage();
        }
    }
    else if (p.equalP(PSTR("settings")))
    {
        //If has no option, return
        if (!p.hasNext())
            return;

        while (p.hasNext())
        {
            p.getNext();

            if (p.equalP(PSTR("--accel")) || p.equalP(PSTR("-a")) || p.equalP(PSTR("--all")))
            {
                HOST_SERIAL.print(F("accel "));
                HOST_SERIAL.println(currentSettings.acceleration);
            }
            if (p.equalP(PSTR("--i2cAddr")) || p.equalP(PSTR("-i")) || p.equalP(PSTR("--all")))
            {
                HOST_SERIAL.print(F("i2cAddr "));
                HOST_SERIAL.println(currentSettings.i2cAddress);
            }
            // if (p.equalP(PSTR("--mode")) || p.equalP(PSTR("-mo")) || p.equalP(PSTR("--all")))
            // {
            //     HOST_SERIAL.print(F("mode "));
            //     HOST_SERIAL.println(currentSettings.mode);
            // }
            // if (p.equalP(PSTR("--algorithm")) || p.equalP(PSTR("-al")) || p.equalP(PSTR("--all")))
            // {
            //     HOST_SERIAL.print(F("algorithm "));
            //     HOST_SERIAL.println(currentSettings.algorithm);
            // }
            if (p.equalP(PSTR("--microstepping")) || p.equalP(PSTR("-m")) || p.equalP(PSTR("--all")))
            {
                HOST_SERIAL.print(F("microstepping "));
                HOST_SERIAL.println(currentSettings.microstepping);
            }
            if(p.equalP(PSTR("--refForwardS")) || p.equalP(PSTR("-rfs")) || p.equalP(PSTR("--all"))){
                HOST_SERIAL.print(F("refForwardS "));
                HOST_SERIAL.println(currentSettings.referenceForwardSpeed);
            }
            if(p.equalP(PSTR("--refBackwardS")) || p.equalP(PSTR("-rbs")) || p.equalP(PSTR("--all"))){
                HOST_SERIAL.print(F("refBackwardS "));
                HOST_SERIAL.println(currentSettings.referenceBackwardSpeed);
            }
            if(p.equalP(PSTR("--refBackwardV")) || p.equalP(PSTR("-rbv")) || p.equalP(PSTR("--all"))){
                HOST_SERIAL.print(F("refBackwardV "));
                HOST_SERIAL.println(currentSettings.referenceBackwardVector);
            }
            if(p.equalP(PSTR("--refAfterPos")) || p.equalP(PSTR("-rap")) || p.equalP(PSTR("--all"))){
                HOST_SERIAL.print(F("refAfterPos "));
                HOST_SERIAL.println(currentSettings.referenceAfterPos);
            }
            if(p.equalP(PSTR("--endLogicTrue")) || p.equalP(PSTR("-elt")) || p.equalP(PSTR("--all"))){
                HOST_SERIAL.print(F("endLogicTrue "));
                HOST_SERIAL.println(currentSettings.endLogicTrue);
            }
            if(p.equalP(PSTR("--refOnStart")) || p.equalP(PSTR("-ros")) || p.equalP(PSTR("--all"))){
                HOST_SERIAL.print(F("refOnStart "));
                HOST_SERIAL.println(currentSettings.referenceOnStart);
            }
            if(p.equalP(PSTR("--refMoveDir")) || p.equalP(PSTR("-rmd")) || p.equalP(PSTR("--all"))){
                HOST_SERIAL.print(F("refMoveDir "));
                HOST_SERIAL.println(currentSettings.referenceMoveDirection);
            }
            if(p.equalP(PSTR("--stopOnLimit")) || p.equalP(PSTR("-sol")) || p.equalP(PSTR("--all"))){
                HOST_SERIAL.print(F("stopOnLimit "));
                HOST_SERIAL.println(currentSettings.stopOnLimit);
            }
            if (p.equalP(PSTR("--recover")))
            {
                settings.acceleration = 10000;
                settings.algorithm = 1;
                settings.i2cAddress = 8;
                settings.microstepping = 0;
                settings.mode = 0;
                settings.referenceAfterPos = 0;
                settings.referenceBackwardVector = 200;
                settings.referenceBackwardSpeed = 500;
                settings.referenceForwardSpeed = 500;
                settings.referenceMoveDirection = -1;
                settings.endLogicTrue = 0;
                settings.referenceOnStart = 0;
                settings.stopOnLimit = 0;
                saveSettings();
                sendOkMessage();
                reset();
            }
        }
    }
    else if (p.equalP(PSTR("reset")))
    {
        //Check options
        if (p.hasNext())
        {
            p.getNext();

            if (p.isOption() && p.equalP(PSTR("--hard")))
            {
                resetHard();
            }
        }
        reset();
    }
    else if (p.equalP(PSTR("check")))
    {
        sendOkMessage();
    }
    else if (p.equalP(PSTR("frequency")))
    {
        HOST_SERIAL.print(F("freq "));
        HOST_SERIAL.println(st_frequency());
    }
    else if (p.equalP(PSTR("rpm")))
    {
        if (!p.hasNext())
        {
            sendErrMessage(ERR_VALUE_NOT_SPECIFIED);
            return;
        }

        if (bit_is_clear(State, MOVING_BIT))
        {
            HOST_SERIAL.println("rpm 0");
            return;
        }

        char *endptr;
        long stepsPerRev = strtol(p.getNext(), &endptr, 10);
        //If number could not be parsed
        if (*endptr != '\0')
        {
            //Send corresponding error code
            sendErrMessage(ERR_NUMBER_PARSE_ERR);
            return; //Don't resume, return
        }
        //Apply microstepping to stepsPerRevolution
        stepsPerRev *= powf(2, currentSettings.microstepping);
        float rpm = st_frequency() / (float)stepsPerRev * 60.0;
        HOST_SERIAL.print("rpm ");
        HOST_SERIAL.println((long)round(rpm));
    }
    else if (p.equalP(PSTR("reference")))
    {
        //If it is already referencing
        if (programState & (REFERENCING_MASK | MOVING_MASK))
        {
            sendErrMessage(ERR_MOVING);
            return; //Return
        }

        //Change program state
        programState |= REFERENCING_MASK;
    }
    else if (p.equalP(PSTR("override")))
    {
        if (!p.hasNext())
        {
            sendErrMessage(ERR_OPTION_NOT_SPECIFIED);
            return;
        }

        while (p.hasNext())
        {
            p.getNext();

            if (p.equalP(PSTR("--speed")) || p.equalP(PSTR("-sp")))
            {
                bool sync =  checkOption(PSTR("--sync") , &p) || checkOption(PSTR("-s") , &p);

                //If argument doesn't specified
                if (!p.hasNext())
                {
                    sendErrMessage(ERR_VALUE_NOT_SPECIFIED);
                    continue;
                }

                //If motor is not moving
                if (bit_is_clear(State, MOVING_BIT))
                {
                    sendErrMessage(ERR_MOTOR_IS_NOT_MOVING); //Send response message
                    continue;
                }

                //If program is reached here
                //It means speed is specified
                p.getNext(); //Retrieve speed

                //Convert string to long
                char *endptr;
                long speed = strtol(p.current(), &endptr, 10);
                //If number could not be parsed
                if (*endptr != '\0')
                {
                    //Send corresponding error code
                    sendErrMessage(ERR_NUMBER_PARSE_ERR);
                    continue; //Don't resume, return
                }
                //Calculate new period
                float period = TIMER_FREQUENCY / (float)speed;
                //Apply new period to plan
                st_plan.target_period = period;

                if(sync){
                    waitForRampingEnd();
                }

                //Send ok message
                sendOkMessage();
            }
        }
    }
    else if (p.equalP(PSTR("rest")))
    {
        //It is not permitted to rest while moving
        if (State & MOVING_MASK)
        {
            //Send err message
            sendErrMessage(ERR_MOVING);
            return; //Dont rest, return
        }

        st_enable(false);
        //Send ok response
        sendOkMessage();
    }
    else if (p.equalP(PSTR("wakeUp")))
    {
        st_enable(true);
        //Respond
        sendOkMessage();
    }
    else if (p.equalP(PSTR("onLimit")))
    {
        if (onLimit())
        {
            HOST_SERIAL.println("yes");
        }
        else
        {
            HOST_SERIAL.println("no");
        }
    }
    else if(p.equalP(PSTR("version"))){
        HOST_SERIAL.println("1.0.0");
    }
    else if (p.equalP(PSTR("help")))
    {
        HOST_SERIAL.println(F("Step1 Control Board, version 1.0"));
        HOST_SERIAL.println(F("Use 'details [--commandName]' to find out more about the commands"));
        HOST_SERIAL.println(F("Available commands : "));
        HOST_SERIAL.println(F("A start(*) means it is required, otherwise optional"));
        HOST_SERIAL.println(F("\tmove [--sync] [--rest] [position*] [speed*] (e.g move --sync 12500 3000)"));
        HOST_SERIAL.println(F("\tjog [--sync] [--rest] [vector*] [speed*] (e.g jog --sync 5000 3000)"));
        HOST_SERIAL.println(F("\tpos"));
        HOST_SERIAL.println(F("\tset ([name]* [value*])+ (e.g set --microstepping 0)"));
        HOST_SERIAL.println(F("\trunning"));
        HOST_SERIAL.println(F("\tstop [--soft] [--sync] (e.g stop --soft --sync)"));
        HOST_SERIAL.println(F("\tsettings [an option*] (e.g settings --all) (e.g settings --recover) (e.g settings accel)"));
        HOST_SERIAL.println(F("\treset [--hard] (e.g reset) (e.g reset --hard)"));
        HOST_SERIAL.println(F("\tcheck"));
        HOST_SERIAL.println(F("\tfrequency"));
        HOST_SERIAL.println(F("\trpm [stepsPerRevolution*]"));
        HOST_SERIAL.println(F("\treference"));
        HOST_SERIAL.println(F("\toverride [an option*] (e.g override --speed 10000)"));
        HOST_SERIAL.println(F("\trest"));
        HOST_SERIAL.println(F("\twakeUp"));
        HOST_SERIAL.println(F("\tonLimit"));
        HOST_SERIAL.println(F("\tversion"));
    }
    else if (p.equalP(PSTR("details")))
    {
        //If no option is specified
        if (!p.hasNext())
        {
            sendErrMessage(ERR_OPTION_NOT_SPECIFIED);
            return;
        }

        //Retrieve option
        p.getNext();

        if (p.equalP(PSTR("move")))
        {
            HOST_SERIAL.println(F("Moves stepper motor to specified [position] with specified [speed]."));
            HOST_SERIAL.println(F("If --sync is specified, it waits for motion to finish and after returns OK message"));
            HOST_SERIAL.println(F("if --sync is not specified, it returns OK message immediately"));
        }
        else if (p.equalP(PSTR("jog")))
        {
            HOST_SERIAL.println(F("Moves amount of [vector] with specified [speed]."));
            HOST_SERIAL.println(F("If --sync is specified, it waits for motion to finish end and after returns OK message"));
            HOST_SERIAL.println(F("if --sync is not specified, it returns OK message immediately"));
        }
        else if (p.equalP(PSTR("pos")))
        {
            HOST_SERIAL.println(F("Returns current position."));
        }
        else if (p.equalP(PSTR("set")))
        {
            HOST_SERIAL.println(F("Sets the specified [value] to [name]. In some values, user can specify options like [--temporary]"));
            HOST_SERIAL.println(F("Multiple value and names can be specified at once"));
        }
        else if (p.equalP(PSTR("running")))
        {
            HOST_SERIAL.println(F("Returns 'yes' if motor is moving, 'no' is not moving"));
        }
        else if (p.equalP(PSTR("stop")))
        {
            HOST_SERIAL.println(F("Stops the stepper motor."));
            HOST_SERIAL.println(F("If no options specified, it stops immediately."));
            HOST_SERIAL.println(F("If --soft option is specified, it stops with decelerating. --sync option can be used with."));
        }
        else if (p.equalP(PSTR("settings")))
        {
            HOST_SERIAL.println(F("Returns settings names and values"));
            HOST_SERIAL.println(F("If it is used with --recover option, it resets to factory settings."));
            HOST_SERIAL.println(F("--all option returns every values"));
        }
        else if (p.equalP(PSTR("reset")))
        {
            HOST_SERIAL.println(F("Resets the program and values"));
            HOST_SERIAL.println(F("if it is used with --hard option, it resets the hardware."));
        }
        else if (p.equalP(PSTR("check")))
        {
            HOST_SERIAL.println(F("Returns ';', it can be used to handshake"));
        }
        else if (p.equalP(PSTR("frequency")))
        {
            HOST_SERIAL.println(F("Returns frequency of pulses. Its unit is Hertz(hz). It can be used for rpm calculations."));
        }
        else if(p.equalP(PSTR("rpm"))){
            HOST_SERIAL.println(F("Combines microstepping and steps per revolution parameters and calculates instant rpm."));
        }
        else if (p.equalP(PSTR("reference")))
        {
            HOST_SERIAL.println(F("Does homing. Limit sensor should be connected. It goes to infinite in absence of signal."));
        }
        else if (p.equalP(PSTR("override")))
        {
            HOST_SERIAL.println(F("Override values."));
            HOST_SERIAL.println(F("If --speed option is specified, change it's speed with accelerating/decelerating."));
        }
        else if (p.equalP(PSTR("rest")))
        {
            HOST_SERIAL.println(F("Disables stepper motor. Cuts current which bound for stepper motor."));
        }
        else if (p.equalP(PSTR("wakeUp")))
        {
            HOST_SERIAL.println(F("Enables stepper motor. Allow current to pass"));
        }
        else if(p.equalP(PSTR("onLimit")))
        {
            HOST_SERIAL.println(F("Returns 'yes' if end sensor is being triggered, otherwise 'no'"));
        }
        else if(p.equalP(PSTR("version"))){
            HOST_SERIAL.println(F("Returns firmware version."));
        }
    }

}

void sendOkMessage()
{
    HOST_SERIAL.println(OK_MESSAGE);
}

void sendErrMessage(int errCode)
{
    HOST_SERIAL.print(F("ERR:"));
    HOST_SERIAL.println(errCode);
}

void stop(bool soft)
{
    //If motor is not moving
    if (!(State & MOVING_MASK))
    {
        return;
    }

    //If option is enabled. then stopping with decelerate
    if (soft)
    {
        st_stop(true);
    }
    else
    {
        // st_plan.steps_remaining = 0;
        st_stop(false);
    }
}

void reset()
{
    //Reset position
    stop(false);
    st_enable(true); //Enabling steppers
    st.posInSteps = 0;
    State &= ~(MOVING_MASK | REFERENCING_MASK);
    // DDRB &= ~(1 << PB0);
    Wire.begin((uint8_t)currentSettings.i2cAddress);
    digitalWrite(A4, INPUT_PULLUP); //Disable internal-pullup
    digitalWrite(A5, INPUT_PULLUP); //Disable internal-pullup
    pinMode(8 , INPUT_PULLUP);

    Wire.onReceive(onReceiveEvent);
    Wire.onRequest(onRequestEvent);
    applyMicrostepping(currentSettings.microstepping);
    st.setAccel(currentSettings.acceleration);
    st.posInSteps = 0;
    st.dirMask = (1 << PD3);
    st.pulseMask = (1 << PD2);
    st.enableMask = (1 << PD7);

    st_set_stepper(&st);
    HOST_SERIAL.println(F("[Step-1 v1.0]"));
}

void applyMicrostepping(int microstep)
{
    PORTD &= ~((1 << MS1) | (1 << MS2) | (1 << MS3));
    PORTD |= microstepTable[microstep];
}

void onReceiveEvent(int bytes)
{
    while (Wire.available())
    {
        int c = Wire.read();
        sh.feed(c);
    }
}

void onRequestEvent()
{
    Wire.write(bit_is_set(State, MOVING_BIT) || bit_is_set(programState, REFERENCING_BIT) ? 1 : 0);
    union {
        byte b[4];
        long num;
    } data;

    data.num = st.posInSteps;

    Wire.write(data.b[0]);
    Wire.write(data.b[1]);
    Wire.write(data.b[2]);
    Wire.write(data.b[3]);
}

void resetHard()
{
    // wdt_enable(WDTO_15MS);

    // while (1)
    // {
    // }
    asm volatile("  jmp 0");
}

void jog(long magnitude, long speed, long opts)
{
    st_move(st.posInSteps + magnitude, speed);

    if (opts & OPTS_SYNC)
    {
        waitForMotionEnd();
    }
}

bool reference()
{
    uint8_t backup = currentSettings.stopOnLimit;
    currentSettings.stopOnLimit = false;
    jog(1000000000L * currentSettings.referenceMoveDirection, currentSettings.referenceForwardSpeed);

    //Jog while limit isn't triggered
    while (!onLimit())
    {
        st_calculate();
        sh2.listen();
    }
    st_stop(false);

    delay(50);

    jog(-currentSettings.referenceMoveDirection * currentSettings.referenceBackwardVector, currentSettings.referenceBackwardSpeed, OPTS_SYNC);

    delay(100);

    //If limit is still trigered after reference backward vector
    //Return false
    if (onLimit())
    {
        currentSettings.stopOnLimit = backup;
        return false;
    }

    st.posInSteps = currentSettings.referenceAfterPos;

    currentSettings.stopOnLimit = backup;
    return true;
}

void waitForMotionEnd()
{
    //Wait for moving bit clear
    while (bit_is_set(State, MOVING_BIT))
    {
        st_calculate();
        sh2.listen(); //Allow other command while waiting
    }
}

void waitForRampingEnd(){
    while(!calc_req){
        delayMicroseconds(10);
    }

    if(st_plan.steps_remaining == 0)
        return;

    //Wait for moving bit clear
    do {
        // HOST_SERIAL.println("Wait for ramping end.");
        st_calculate();
        sh2.listen(); //Allow other command while waiting
    }
    while (bit_is_set(State, RAMPING_BIT));
}

bool checkTempOption(Parser *p)
{
    //Holds temp option flag
    bool temporary = false;
    //Check next word existence
    if (p->hasNext())
    {
        //If it is temp option
        if (p->equalP(p->peek(), PSTR("--temp")) || p->equalP(p->peek(), PSTR("-t")))
        {
            temporary = true; //Set flag true
            p->getNext();
        }
    }

    return temporary;
}

bool checkOption(const char* strP , Parser* p){
        //Holds temp option flag
    bool exist = false;
    //Check next word existence
    if (p->hasNext())
    {
        //If it is temp option
        if (p->equalP(p->peek(), strP))
        {
            exist = true; //Set flag true
            p->getNext();
        }
    }

    return exist;
}

//It tests something
void test(){
    st_plan.steps_remaining = 10000;
    long start , time , sum = 0;
    long highest = 0;
    long lowest = 100000000;
    unsigned long loopCount = 10000;
    currentSettings.stopOnLimit = false;
    for(unsigned int i = 0; i < loopCount; i++){
        start = micros();
        st_pulse();
        st_calculate();
        time = micros() - start - 6;
        sum += time;

        if(time > highest)
            highest = time;
        
        if(time < lowest)
            lowest = time;
    }

    float average = sum / (float)loopCount;

    Serial.print("Average\t");
    Serial.print(average);
    Serial.print("us");
    Serial.print("\tHighest\t");
    Serial.print(highest);
    Serial.print("us");
    Serial.print("\tLowest\t");
    Serial.print(lowest);
    Serial.println("us");
}